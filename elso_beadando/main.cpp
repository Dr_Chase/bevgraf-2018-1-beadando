#include <GL/glut.h>
#include <bevgrafmath2017.h>

#include <cmath>
#include <random>
#include <iostream>
#include <chrono>
#include <thread>
#include <cstring>
#include <string>
#include <cstddef>
#include <utility>

GLsizei  const winWidth = 600,  const winHeight = 600;
GLsizei const winWidthZero = 0, const winHeightZero = 0;
const double EPSYLON_D = 0.0001; // for double comparison
const float EPSYLON_F = 0.0001; // or 

//game logic
int32_t nEatenPoints = 0;
int32_t nMaxEdiblePoints = 10;
bool bWinningConditionsMet = false;


//for keyboard handling
GLint keyStates[256];

// double equalisation 
bool equalsTwoFloat(float first, float second, float epsylon) {
	return fabs(first - second) < epsylon;
}

bool equalsTwoFloat(float first, float second) {
	return equalsTwoFloat(first, second, EPSYLON_F);
}

bool equalsTwoDouble(double first, double second, double epsylon) {
	return fabs(first - second) < epsylon;
}

bool equalsTwoDouble(double first, double second) {
	return equalsTwoDouble(first, second, EPSYLON_D);
}

void drawCircle(vec2 O, GLdouble r) {

	glBegin(GL_LINE_LOOP);
	for (GLdouble t = 0; t <= 2 * pi(); t += 0.01)
		glVertex2d(O.x + r * cos(t), O.y + r * sin(t));
	glEnd();
}

// my edible point and it's random generator
vec2 myEdiblePoint;
GLubyte uns8PointRedColor;
GLubyte uns8PointGreenColor;
GLubyte uns8PointBlueColor;

GLfloat pointSize;

std::random_device rd;
std::mt19937 mersenneTwisterRandomGenerator(rd());
std::uniform_int_distribution<int> distributeForWinHeight(0, winHeight);
std::uniform_int_distribution<int> distributeForWinWidth(0, winWidth);

// 0- 200 ig mert egy�bk�nt t�l vil�gos lenne a sz�n, �s akkor nem biztos hogy j�l l�tsz�dna
std::uniform_int_distribution<int> distributeForPointColor(0, 200);

// my belowed circle
vec2 speedAndDirectionOfCircle;
vec2 origoOfCircle;
GLdouble rayOfCircle;
double dCircleBellyGrowmentAfterEating;

// edible point functions
void generatePointColor() {
	uns8PointRedColor = distributeForPointColor(mersenneTwisterRandomGenerator);
	uns8PointGreenColor = distributeForPointColor(mersenneTwisterRandomGenerator);
	uns8PointBlueColor = distributeForPointColor(mersenneTwisterRandomGenerator);
}
void generateNewRandomPoint() {
	generatePointColor();
	myEdiblePoint.x = distributeForWinHeight(mersenneTwisterRandomGenerator);
	myEdiblePoint.y = distributeForWinWidth(mersenneTwisterRandomGenerator);

	if (
			(
				(
					(myEdiblePoint.x < (winWidthZero + ceil((GLint)rayOfCircle)))
					&&
					(
						(myEdiblePoint.y < (winHeightZero + ceil((GLint)rayOfCircle)))
						||
						(myEdiblePoint.y > (winHeight - ceil((GLint)rayOfCircle)))
					)
				)
				||

				(
					(myEdiblePoint.x >(winWidthZero - ceil((GLint)rayOfCircle)))	
						&&
						(
							(myEdiblePoint.y < (winHeightZero +  ceil((GLint)rayOfCircle)))	
								||
							(myEdiblePoint.y >(winHeight - ceil((GLint)rayOfCircle)))
						)
				)
			) || dist(myEdiblePoint, origoOfCircle) <= rayOfCircle

		)
	{
		generateNewRandomPoint();
	}
}

void drawPoint() {
	glColor3ub(uns8PointRedColor, uns8PointGreenColor, uns8PointBlueColor);
	glBegin(GL_POINTS);
	
		glVertex2f(myEdiblePoint.x, myEdiblePoint.y);
	glEnd();
}

void initPoint() {
	pointSize = 10.0;
	generateNewRandomPoint();
}


// circle functions
void initCircle() {
	speedAndDirectionOfCircle.x = 1.0;
	speedAndDirectionOfCircle.y = 1.0;
	origoOfCircle.x = (float)(winWidth / 2);
	origoOfCircle.y = (float)(winHeight / 2);
	rayOfCircle = 30.0;
	dCircleBellyGrowmentAfterEating = 2.0;
}

void stopCircle() {
	speedAndDirectionOfCircle.x = 0.0;
	speedAndDirectionOfCircle.y = 0.0;
}

void drawCircle() {
	drawCircle(origoOfCircle, rayOfCircle);
}

// for drawing out text
void drawText(float x, float y, void *font, char *string) {
	char *c;
	glRasterPos2f(x, y);
	int len = (int)strlen(string);

	for (int i = 0; i < len; i++) {
		glutBitmapCharacter(font, string[i]);
	}
}

char*  createPointCSTRing() {
	std::string strMessage = "Pontok: ";
	std::string strNumOfPoints = std::to_string(nEatenPoints);
	strMessage = strMessage + strNumOfPoints;

	char const *pchar = strMessage.c_str();
	char *output = _strdup(pchar);
	//char *output = (char*)pchar;
	return output;
}

// keyboard handling
void keyPressed(unsigned char key, int x, int y)
{
	keyStates[key] = 1;
}

void keyUp(unsigned char key, int x, int y)
{
	keyStates[key] = 0;
	if (key == 'a') {
		std::swap(speedAndDirectionOfCircle.x, speedAndDirectionOfCircle.y);
		speedAndDirectionOfCircle.y = -speedAndDirectionOfCircle.y;
	}
	if (key == 'd') {
		std::swap(speedAndDirectionOfCircle.x, speedAndDirectionOfCircle.y);
		speedAndDirectionOfCircle.x = -speedAndDirectionOfCircle.x;
	}

}
// Keyboard
void keyboard( /*unsigned char key, int x, int y */)
{

	if (keyStates[27]) { exit(0); }

}

void init()
{
	initCircle();
	initPoint();

	glClearColor(1.0, 1.0, 1.0, 0.0);
	glMatrixMode(GL_PROJECTION);
	gluOrtho2D(0.0, winWidth, 0.0, winHeight);
	glShadeModel(GL_FLAT);
	glEnable(GL_POINT_SMOOTH);
	glPointSize(pointSize);
}

void display()
{
	if (!bWinningConditionsMet) {

		glClear(GL_COLOR_BUFFER_BIT);

		drawText((float)(winWidth - 100), 0.5, GLUT_BITMAP_HELVETICA_18, createPointCSTRing());


		glColor3f(0.0, 0.0, 1.0);
		drawCircle();

		drawPoint();
		glutSwapBuffers();
	}
	else
	{
		glClear(GL_COLOR_BUFFER_BIT);
		drawText( (float) (winWidth/2), (double) (winHeight / 2),  GLUT_BITMAP_HELVETICA_18, "Nyertel!");
		glutSwapBuffers();
	}
	
}

void update(int n)
{
	keyboard();

	origoOfCircle.x = origoOfCircle.x + speedAndDirectionOfCircle.x;
	origoOfCircle.y = origoOfCircle.y + speedAndDirectionOfCircle.y;

	if ( (origoOfCircle.x + rayOfCircle) > winWidth) {
		speedAndDirectionOfCircle.x =  (-1) * abs(speedAndDirectionOfCircle.x);
	}

	if ((origoOfCircle.x - rayOfCircle) < 0) {
		speedAndDirectionOfCircle.x = abs(speedAndDirectionOfCircle.x);
	}
	
	if ((origoOfCircle.y + rayOfCircle) > winHeight) {
		speedAndDirectionOfCircle.y = (-1) * abs(speedAndDirectionOfCircle.y);
	}

	if ((origoOfCircle.y - rayOfCircle) < 0) {
		speedAndDirectionOfCircle.y = abs(speedAndDirectionOfCircle.y);
	}

	if (islessequal(dist(origoOfCircle, myEdiblePoint), rayOfCircle) ) {
		generateNewRandomPoint();
		rayOfCircle = rayOfCircle + dCircleBellyGrowmentAfterEating;
		nEatenPoints++;
	}

	//game logic
	if (nEatenPoints >= nMaxEdiblePoints) {
		bWinningConditionsMet = true;
	}

	if (bWinningConditionsMet) {
		stopCircle();
	}

	//std::cout << "Origo x : " << origoOfCircle.x << "\n";
	//std::cout << "Origo y : " << origoOfCircle.y << "\n";
	//std::cout << "speedAndDirectionOfCircle x : " << origoOfCircle.x << "\n";
	//std::cout << "speedAndDirectionOfCircle y : " << origoOfCircle.y << "\n";

	glutPostRedisplay();

	glutTimerFunc(5, update, 0);

}



int main(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(winWidth, winHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow(argv[0]);
	init();
	glutDisplayFunc(display);
	glutKeyboardFunc(keyPressed);
	glutKeyboardUpFunc(keyUp);

	glutTimerFunc(5, update, 0);

	glutMainLoop();
	return 0;
}
